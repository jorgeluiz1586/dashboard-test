const data = {
  amount: 120,
  max_level: 400,
  title: 'Você está no nível 1',
  message: 'Cada 100 pontos equivalem a R$10,00',
  infos: [
    {
      level: 'Nivel 1',
      descriptions: ['bronze'],
      forecolor: '#000',
      backcolor: '#FF5D5D',
      min_points: 401,
    },
    {
      level: 'Nivel2',
      descriptions: ['Ouro'],
      forecolor: '#000',
      backcolor: '#00984C',
      min_points: 1200,
    },
    {
      level: 'Nivel4',
      descriptions: ['cobre'],
      forecolor: '#000',
      backcolor: '#d1d147',
      min_points: 2000,
    },
    {
      level: 'Nivel5',
      descriptions: ['latão'],
      forecolor: '#000',
      backcolor: '#00954C',
      min_points: 2000,
    },
  ],
};

export default data;
