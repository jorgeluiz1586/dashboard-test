import { createStore } from 'vuex';

import scoreModule from './score-module';

const store = createStore({
  modules: {
    scoreModule: scoreModule,
  },
});

export default store;
