import { ActionContext } from 'vuex';
import { api } from 'src/boot/axios';

export type ScoreActions = {
  fetchScore: (context: ActionContext<ScoreActions, unknown>) => void;
};

export default {
  async fetchScore(context) {
    await api.get('/test/hie').then((response) => {
      context.commit('setScore', response.data);
    });
  },
} as ScoreActions;
