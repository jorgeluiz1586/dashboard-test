export type ScoreState = {
  score: object;
};

export type StockOverviewMutations = {
  setScore: (state: ScoreState, payload: object) => void;
};

export default {
  setScore(state, payload) {
    state.score = { ...payload };
  },
} as StockOverviewMutations;
