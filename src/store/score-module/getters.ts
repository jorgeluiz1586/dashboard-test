export type ScoreState = {
  score: object;
};

export type ScoreGetters = {
  getScore: (state: ScoreState) => object;
};

export default {
  getScore(state) {
    return state.score;
  },
} as ScoreGetters;
